package ukdw.com.progmob_2020.pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.RecView);
        MahasiswaRecyclerAdapter mhsAdapter;
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        Mahasiswa m1 = new Mahasiswa("Nikita Semben", "72180237", "08133334290");
        Mahasiswa m2 = new Mahasiswa("Modesta", "72180208", "082190087009");
        Mahasiswa m3 = new Mahasiswa("Brigita", "72180221", "08156334290");
        Mahasiswa m4 = new Mahasiswa("Diory", "72180266", "08170334290");
        Mahasiswa m5 = new Mahasiswa("Silvia", "72180265", "08239334290");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mhsAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mhsAdapter.setMhsList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mhsAdapter);
    }
}