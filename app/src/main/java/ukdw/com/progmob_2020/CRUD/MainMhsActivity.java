package ukdw.com.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;


public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);
        Button btnGet = (Button)findViewById(R.id.btnmhs);
        Button btnAdd = (Button)findViewById(R.id.btnAdd);
        Button btnEd = (Button)findViewById(R.id.btnUp);
        Button btnDel = (Button)findViewById(R.id.btnDel);


        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(in);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(in);
            }
        });
        btnEd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainMhsActivity.this, MahhasiswaUpdateActivity.class);
                startActivity(in);
            }
        });
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainMhsActivity.this, MahasiswaDeleteActivity.class);
                startActivity(in);
            }
        });
    }
}