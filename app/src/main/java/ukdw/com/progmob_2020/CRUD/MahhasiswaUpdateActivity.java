package ukdw.com.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahhasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahhasiswa_update);
        EditText edNim = (EditText)findViewById(R.id.editNim);
        EditText edNimCr = (EditText)findViewById(R.id.editNimCr);
        EditText EdNama = (EditText)findViewById(R.id.editNama);
        EditText EdAlamat = (EditText)findViewById(R.id.editAlamat);
        EditText EdEmail = (EditText)findViewById(R.id.editEmail);
        Button btnUp = (Button) findViewById(R.id.btnSimpan);

        pd = new ProgressDialog(MahhasiswaUpdateActivity.this);

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon bersabar... ini ujian");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> calldel = service.delete_mhs(edNimCr.getText().toString(), "72180237");
                calldel.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        //pd.dismiss();
                        Toast.makeText(MahhasiswaUpdateActivity.this, "Update Sukses", Toast.LENGTH_LONG).show();

                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahhasiswaUpdateActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> calladd = service.add_mhs(
                        EdNama.getText().toString(),
                        edNim.getText().toString(),
                        EdAlamat.getText().toString(),
                        EdEmail.getText().toString(),
                        "seadanya",
                        "72180237");
                calladd.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahhasiswaUpdateActivity.this, "Update Sukses", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(MahhasiswaUpdateActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}