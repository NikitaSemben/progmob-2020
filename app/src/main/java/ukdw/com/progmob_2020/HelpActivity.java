package ukdw.com.progmob_2020;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        TextView txthlp = (TextView)findViewById(R.id.textHelp);

        Bundle bn = getIntent().getExtras();
        String textHelp = bn.getString("help_String" );
        txthlp.setText(textHelp);
    }
}