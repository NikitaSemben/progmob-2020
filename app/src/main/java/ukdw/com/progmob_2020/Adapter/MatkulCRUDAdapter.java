package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.Matakuliah;
import ukdw.com.progmob_2020.R;

public class MatkulCRUDAdapter extends RecyclerView.Adapter<MatkulCRUDAdapter.ViewHolder> {
    private Context context;
    private List<Matakuliah> MatakuliahList;

    public MatkulCRUDAdapter(Context context) {
        this.context = context;
    }

    public MatkulCRUDAdapter(List<Matakuliah> matakuliahList) {
        MatakuliahList = matakuliahList;
    }

    public List<Matakuliah> getMatakuliahList() {
        return MatakuliahList;
    }

    public void setMatakuliahList(List<Matakuliah> matakuliahList) {
        MatakuliahList = matakuliahList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MatkulCRUDAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull MatkulCRUDAdapter.ViewHolder holder, int position) {
        Matakuliah b = MatakuliahList.get(position);
        holder.txtNama.setText(b.getNama());
        holder.txtNim.setText(b.getKode());
        holder.txtAlamat.setText(b.getHari());
        holder.txtEmail.setText(b.getSesi());
        holder.txtNohp.setText(b.getSks());
    }

    @Override
    public int getItemCount() {
        return MatakuliahList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNim, txtNohp, txtAlamat, txtEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtNim = itemView.findViewById(R.id.txtNim);
            txtNohp = itemView.findViewById(R.id.txtNohp);
            txtAlamat = itemView.findViewById(R.id.txtAlamat);
            txtEmail = itemView.findViewById(R.id.txtEmail);

        }
    }
}
