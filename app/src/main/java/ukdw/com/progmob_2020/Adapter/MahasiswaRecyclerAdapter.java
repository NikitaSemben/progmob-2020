package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class MahasiswaRecyclerAdapter extends RecyclerView.Adapter<MahasiswaRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Mahasiswa>MhsList;

    public MahasiswaRecyclerAdapter(Context context) {
        this.context = context;
        MhsList = new ArrayList<>();
    }
    public List<Mahasiswa> getMhsList() {
        return MhsList;
    }

    public void setMhsList(List<Mahasiswa> mhsList) {
        this.MhsList = mhsList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_cardview, parent, false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = MhsList.get(position);
        holder.txtNama.setText(m.getNama());
        holder.txtNim.setText(m.getNim());
        holder.txtNohp.setText(m.getNoHp());
    }
    @Override
    public int getItemCount() {
        return MhsList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNim, txtNohp;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtNim = itemView.findViewById(R.id.txtNim);
            txtNohp = itemView.findViewById(R.id.txtNohp);
        }
    }
}
