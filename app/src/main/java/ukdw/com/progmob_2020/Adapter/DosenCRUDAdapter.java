package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.R;

public class DosenCRUDAdapter extends RecyclerView.Adapter<DosenCRUDAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public DosenCRUDAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenCRUDAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DosenCRUDAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DosenCRUDAdapter.ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.txtNama.setText(d.getNama());
        holder.txtNim.setText(d.getNidn());
        holder.txtAlamat.setText(d.getAlamat());
        holder.txtEmail.setText(d.getEmail());
        holder.txtNohp.setText(d.getGelar());
    }
    @Override
    public int getItemCount() {
        return dosenList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNim, txtNohp ,txtAlamat, txtEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtNim = itemView.findViewById(R.id.txtNim);
            txtNohp = itemView.findViewById(R.id.txtNohp);
            txtAlamat = itemView.findViewById(R.id.txtAlamat);
            txtEmail = itemView.findViewById(R.id.txtEmail);
        }
    }
}
