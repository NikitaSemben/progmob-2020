package ukdw.com.progmob_2020.pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);
        Button btnGet = (Button)findViewById(R.id.btnDosen);
        Button btnAdd = (Button)findViewById(R.id.btnAdd);
        Button btnEd = (Button)findViewById(R.id.btnUp);
        Button btnDel = (Button)findViewById(R.id.btnDel);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainDosenActivity.this, ListDosenActivity.class);
                startActivity(in);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(in);
            }
        });
        btnEd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainDosenActivity.this, UpdateDosenActivity.class);
                startActivity(in);
            }
        });
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainDosenActivity.this, DeleteDosenActivity.class);
                startActivity(in);
            }
        });
    }
}