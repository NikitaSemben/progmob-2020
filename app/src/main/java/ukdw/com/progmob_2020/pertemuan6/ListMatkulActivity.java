package ukdw.com.progmob_2020.pertemuan6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.DosenCRUDAdapter;
import ukdw.com.progmob_2020.Adapter.MatkulCRUDAdapter;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Model.Matakuliah;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class ListMatkulActivity extends AppCompatActivity {
    RecyclerView rvMat;
    MatkulCRUDAdapter matAdapter;
    ProgressDialog pd;
    List<Matakuliah> matakuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_matkul);

        rvMat = (RecyclerView)findViewById(R.id.rvMatkulAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matakuliah>> call = service.getMatkul("72180237");

        call.enqueue(new Callback<List<Matakuliah>>() {
            @Override
            public void onResponse(Call<List<Matakuliah>> call, Response<List<Matakuliah>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                matAdapter = new MatkulCRUDAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ListMatkulActivity.this);
                rvMat.setLayoutManager(layoutManager);
                rvMat.setAdapter(matAdapter);

            }

            @Override
            public void onFailure(Call<List<Matakuliah>> call, Throwable t) {
                Toast.makeText(ListMatkulActivity.this, "Error", Toast.LENGTH_LONG);
            }
        });
    }
}