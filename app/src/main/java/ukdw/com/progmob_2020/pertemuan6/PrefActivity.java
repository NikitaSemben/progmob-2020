package ukdw.com.progmob_2020.pertemuan6;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.Network.UtilsApi;
import ukdw.com.progmob_2020.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";
    EditText nim;
    EditText password;
    Button login;
    private String KEY_NAME = "NAMA";
    ConstraintLayout currentLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);
        nim = (EditText)findViewById(R.id.edNim);
        password = (EditText)findViewById(R.id.edPassword);
        login = (Button)findViewById(R.id.btnLog);
        Button btnPref = (Button)findViewById(R.id.btnLog);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String myNim = nim.getText().toString();
                String myPassword = password.getText().toString();
//                try {
                if (myNim.isEmpty()) {
                    nim.setError("Email Empty");
                    return;
                }
                if (myPassword.isEmpty()) {
                    password.setError("Password Empty");
                    return;
                }
                doLogin(nim, password);
            }
        });

        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin","0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    btnPref.setText("Logout");
                }else{
                    editor.putString("isLogin","0");
                    btnPref.setText("Login");
                }
                editor.commit();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.progmob,menu);
        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_bar:
                currentLayout = (ConstraintLayout)findViewById(R.id.constrainLayoutPref);
                //Toast.makeText(this, "Tambah Sesuatu", Toast.LENGTH_LONG).show();
                Snackbar.make(currentLayout, "Tertekan Sesuatu",Snackbar.LENGTH_LONG).show();
                return true;
            case (R.id.edit_bar):
                Snackbar.make(currentLayout, "Tekan Edit",Snackbar.LENGTH_LONG).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void doLogin(final EditText myNimnik, EditText myPassword) {
//        nimnik.setEnabled(false);
//        password.setEnabled(false);
        login.setEnabled(false);
        String authorization = "Basic " + UtilsApi.base64Encode(nim, password);
        RetrofitClientInstance.ServiceClient
                .buildServiceClient()
                .login(authorization)
                .enqueue(new Callback<AuthorizationResponse>() {

                    @Override
                    public void onResponse(Call<AuthorizationResponse> call, Response<AuthorizationResponse> response) {
                        if (response.isSuccessful()) {

                            AuthorizationResponse auth = response.body();
                            UserPreferences.setUserId(PrefActivity.this, auth.getLoggedInUser().getId());
                            UserPreferences.hasLogin(PrefActivity.this);

                            startActivity(new Intent(PrefActivity.this, MainAllActivity.class));
                            finish();

                        } else {
                            Toast.makeText(PrefActivity.this, "Unknown login or wrong password for login " + nim, Toast.LENGTH_SHORT).show();
                            Log.d("onResponse", "onResponse: " + response.message());

//                            nimnik.setEnabled(true);
//                            password.setEnabled(true);
                            login.setEnabled(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<AuthorizationResponse> call, Throwable t) {
                        Toast.makeText(PrefActivity.this, "Something Wrong!", Toast.LENGTH_SHORT).show();
                        Log.d("onFailure", "onFailure: " + t.getMessage());
                        login.setEnabled(true);
                    }

                });

    }

}