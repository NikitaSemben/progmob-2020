package ukdw.com.progmob_2020.pertemuan6;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ukdw.com.progmob_2020.R;

public class MainAllActivity extends AppCompatActivity {
    private Button btnLogout;
    TextView txtUser;
    String resultNama;
    private String KEY_NAME = "NAMA";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_all);
        SharedPreferences pref = MainAllActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        Button btnMakul = (Button)findViewById(R.id.btnMakul);
        Button btnDosen = (Button)findViewById(R.id.btnDosen);
        Button btnMhs = (Button)findViewById(R.id.btnMhs);

        Bundle extras = getIntent().getExtras();
        if (extras !=null)
            resultNama = extras.getString("result_nama");
            txtUser.setText(resultNama);


        Button btnLogout = (Button)findViewById(R.id.btnLogOut);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    private void showDialog(){
        AlertDialog.Builder pemberitahuan = new AlertDialog.Builder(this);

        pemberitahuan.setTitle("KELUAR DARI SINI?");

        pemberitahuan
                .setMessage("Pilih YA untuk keluar")
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        startActivity(new Intent(MainAllActivity.this, PrefActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = pemberitahuan.create();
        alertDialog.show();

    }

    private void initComponents(){
        txtUser = (TextView) findViewById(R.id.txtUser);
    }

}