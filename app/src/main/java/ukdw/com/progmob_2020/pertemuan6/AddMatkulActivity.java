package ukdw.com.progmob_2020.pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class AddMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_matkul);
        EditText edNama = (EditText)findViewById(R.id.editNama);
        EditText edKode= (EditText)findViewById(R.id.editKode);
        EditText edhari= (EditText)findViewById(R.id.editHari);
        EditText edSesi = (EditText)findViewById(R.id.editSesi);
        EditText edSks = (EditText)findViewById(R.id.editSks);
        Button btnSim = (Button)findViewById(R.id.btnSimpan);


        pd = new ProgressDialog(AddMatkulActivity.this);
        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon bersabar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        edNama.getText().toString(),
                        edKode.getText().toString(),
                        edhari.getText().toString(),
                        edSesi.getText().toString(),
                        edSks.getText().toString(),
                        "72180237"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(AddMatkulActivity.this,"Data Sukses", Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(AddMatkulActivity.this, "Error",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}