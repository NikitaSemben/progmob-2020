package ukdw.com.progmob_2020.pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UpdateMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_matkul);
        EditText edKode = (EditText)findViewById(R.id.editKode);
        EditText edKodeCr = (EditText)findViewById(R.id.editKodeCr);
        EditText EdNama = (EditText)findViewById(R.id.editNama);
        EditText EdSesi = (EditText)findViewById(R.id.editSesi);
        EditText EdHari = (EditText)findViewById(R.id.editHari);
        EditText EdSks = (EditText)findViewById(R.id.editSks);
        Button btnUp = (Button) findViewById(R.id.btnSimpan);

        pd = new ProgressDialog(UpdateMatkulActivity.this);
        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon bersabar... ini ujian");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> calldel = service.delete_matkul(edKodeCr.getText().toString(), "72180237");
                calldel.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        //pd.dismiss();
                        Toast.makeText(UpdateMatkulActivity.this, "Update Sukses", Toast.LENGTH_LONG).show();

                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatkulActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> call = service.update_matkul(
                        edKode.getText().toString(),
                        EdNama.getText().toString(),
                        EdSesi.getText().toString(),
                        EdHari.getText().toString(),
                        EdSks.getText().toString(),
                        "72180237"
                );
            }
        });
    }
}