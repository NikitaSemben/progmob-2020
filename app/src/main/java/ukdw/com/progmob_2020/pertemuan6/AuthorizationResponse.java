package ukdw.com.progmob_2020.pertemuan6;

public class AuthorizationResponse {
    private LoggedInUser loggedInUser;
    private String token;

    public LoggedInUser getLoggedInUser() {
        return loggedInUser;
    }
}
