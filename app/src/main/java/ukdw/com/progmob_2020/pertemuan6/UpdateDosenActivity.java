package ukdw.com.progmob_2020.pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.style.UpdateAppearance;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UpdateDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_dosen);
        EditText edNidn = (EditText)findViewById(R.id.editNidn);
        EditText edNimCr = (EditText)findViewById(R.id.editNidnCr);
        EditText EdNama = (EditText)findViewById(R.id.editNama);
        EditText EdAlamat = (EditText)findViewById(R.id.editAlamat);
        EditText EdEmail = (EditText)findViewById(R.id.editEmail);
        EditText EdGelar = (EditText)findViewById(R.id.editGelar);
        Button btnUp = (Button) findViewById(R.id.btnSimpan);

        pd = new ProgressDialog(UpdateDosenActivity.this);

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon bersabar... ini ujian");
                pd.show();


                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> calldel = service.delete_dosen(edNimCr.getText().toString(), "72180237");
                calldel.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        //pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Update Sukses", Toast.LENGTH_LONG).show();

                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> call = service.update_dosen(
                        edNidn.getText().toString(),
                        EdNama.getText().toString(),
                        edNimCr.getText().toString(),
                        EdAlamat.getText().toString(),
                        EdEmail.getText().toString(),
                        EdGelar.getText().toString(),
                        "72180237"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Update Sukses", Toast.LENGTH_LONG).show();
                        Intent in = new Intent(UpdateDosenActivity.this, MainDosenActivity.class);
                        startActivity(in);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}