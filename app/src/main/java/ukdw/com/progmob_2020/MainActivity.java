package ukdw.com.progmob_2020;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ukdw.com.progmob_2020.pertemuan2.ListActivity;
import ukdw.com.progmob_2020.pertemuan2.RecyclerActivity;
import ukdw.com.progmob_2020.pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        Button btn = (Button)findViewById(R.id.btn1);
        final EditText myedit = (EditText)findViewById(R.id.idText1);
        Button btnhp = (Button)findViewById(R.id.btnLl);
        Button btnTgs = (Button)findViewById(R.id.btnTugas);
        Button btnList = (Button)findViewById(R.id.btnList);
        Button btnRec = (Button)findViewById(R.id.btnRec);
        Button btnCard = (Button)findViewById(R.id.btnCard);
        Button btnDebb = (Button)findViewById(R.id.btnDeb);

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, ListActivity.class);
                startActivity(in);
            }
        });
        btnRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(in);
            }
        });
        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(in);
            }
        });
        btnDebb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(in);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("Coba aja terus", myedit.getText().toString());
                txtView.setText(myedit.getText().toString());
            }
        });
       btnhp.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent in = new Intent(MainActivity.this,HelpActivity.class);
               Bundle bn = new Bundle();

               bn.putString("help_String", myedit.getText().toString());
               in.putExtras(bn);

               startActivity(in);
           }
       });
       btnTgs.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(MainActivity.this,TrackerActivity.class);
               startActivity(i);
           }
       });

        txtView.setText(R.string.txt_yuhuuuu);

    }
}